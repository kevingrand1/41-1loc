.DEFAULT_GOAL := help
.PHONY: help
help: ## Affiche cette aide
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

.PHONY: rollback
rollback: ## Annule la derniere migration
	php bin/console doctrine:migration:migrate prev

.PHONY: test
test: ## Execute les tests
	bin/phpunit --testdox
	php bin/console c:c

.PHONY: lint
lint: ## Analyse le code
	vendor/bin/phpstan analyse --memory-limit=-1
	vendor/bin/yaml-lint config/

.PHONY: format
format: ## Reformate le code
	vendor/bin/phpcbf
	vendor/bin/php-cs-fixer fix --dry-run --diff
	vendor/bin/twigcs templates/

.PHONY: all
all: ## Reformate le code + analyse le code + execute les tests
	vendor/bin/phpcbf
	vendor/bin/php-cs-fixer fix --dry-run --diff
	vendor/bin/twigcs templates/
	vendor/bin/phpstan analyse --memory-limit=-1
	vendor/bin/yaml-lint config/
	bin/phpunit --testdox
	php bin/console c:c