# Tests

* **Test unitaire**: Tester unitairement un ensemble de classes

* **Test fonctionnel**: Tester l'interface en simulant une requete HTTP

## Pour lancer les tests

```
php bin/phpunit --testdox
```


[Retour au sommaire](../README.md)