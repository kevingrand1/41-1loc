# Environnements

## Pré-requis
* PHP >= 7.4
* MySql >= 5.7

il existe plusieurs environnements différents :
* `prod` : Environnement de production (site en ligne)
* `dev` : Environnement de développement
* `test` : Environnement pour les tests

Pour chaque environnement, il sera necessaire de créer un fichier contenant les variables d'environnement.

## Exemples

Pour les tests : `.env.dev` ou `.env.dev.local`

```dotenv
DATABASE_URL="sqlite:///%kernel.project_dir%/var/data.db"
```

[Retour au sommaire](../README.md)