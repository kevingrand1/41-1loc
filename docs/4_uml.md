# UML


* [Cas d'utilisation](#cas-dutilisation)
    * [Gestion des salles](#gestion-des-salles)
* [Diagramme de classe](#diagramme-de-classe)
* [Sitemap](#sitemap)


## Cas d'utilisation
[![Cas d'utilisation](uml/img/usecase_admin.png)](uml/diagramme-cas-utilisation_admin.puml)
[![Cas d'utilisation](uml/img/usecase_client.png)](uml/diagramme-cas-utilisation_client.puml)
[![Cas d'utilisation](uml/img/usecase_visitor.png)](uml/diagramme-cas-utilisation_visiteur.puml)

### Gestion des salles
[![Gestion des salles](uml/img/usecase_gestion-salle.png)](uml/usecase_gestion-salle.puml)

## Diagramme de classe
[![Diagramme de classe](uml/img/ClassDiagram.png)](uml/diagramme-classe.puml)

___
## | Sitemap |
______
## Visiteur
[![Sitmap visiteur](uml/img/sitemap_visiteur.png)](uml/sitemap/sitemap_visiteur.puml)

## Client
[![Sitmap client](uml/img/sitemap_client.png)](uml/sitemap/sitemap_client.puml)

## Administrateur
[![Sitmap employe](uml/img/sitemap_admin.png)](uml/sitemap/sitemap_admin.puml)

[Retour au sommaire](../README.md)
