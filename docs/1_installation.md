# Installation

## Récuperer les sources du projet
```
git@gitlab.com:kevingrand1/41-1loc.git
```

## Pré-requis
* PHP >= 7.4
* Extensions PHP :
* Composer
* MySql >= 5.7
* NodeJS >= 14.15
* npm >= 6.14

## Installer les dépendances
Se positionner dans le dossier du projet :
```
cd <41-1LOC>
```

Executer les commandes suivantes :
```
composer install
yarn install
```

## Initialiser la base de données

Pour l'environnement de `prod` et de `dev` :
```
php bin/console donctrine:database:create
php bin/console donctrine:migration:migrate
```

Pour l'environnement de `test` :
```
php bin/console doctrine:database:create -e test
php bin/console doctrine:schema:update -f -e test
```

Import des données de `test` :
```
php bin/console donctrine:fixtures:load
```

## Lancer la compilation des assets

Compiler une seule fois les fichiers en environnement de développement :
```
npm encore dev
```

Compilation automatique :
```
yarn watch
```

Compilation pour la production :
```
yarn encore production
```

## Lancer le serveur en local
Il est nécessaire d'avoir installé le [binaire de symfony](https://symfony.com/download)

```
symfony serve
```


[Retour au sommaire](../README.md)
