<?php

namespace App\Command;

use App\Entity\Category;
use App\Entity\Image;
use App\Entity\Reservation;
use App\Entity\Room;
use App\Entity\User;
use App\Event\CreateReservationEvent;
use Doctrine\DBAL\Exception;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\ORMException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Filesystem\Exception\IOException;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Yaml\Exception\ParseException;
use Symfony\Component\Yaml\Yaml;

class ImportDataCommand extends Command
{
    protected static $defaultName = 'app:import-data';
    protected static $defaultDescription = 'Importe les données initials de la société 1\'loc depuis un fichier YML et télécharge les images depuis GitLab';
    private KernelInterface $kernel;
    private EntityManagerInterface $em;
    private UserPasswordHasherInterface $passwordHasher;
    private EventDispatcherInterface $dispatcher;

    public function __construct(
        KernelInterface $kernel,
        EntityManagerInterface $em,
        UserPasswordHasherInterface $passwordHasher,
        EventDispatcherInterface $dispatcher,
        string $name = null
    ) {
        parent::__construct($name);
        $this->kernel = $kernel;
        $this->em = $em;
        $this->passwordHasher = $passwordHasher;
        $this->dispatcher = $dispatcher;
    }

    /**
     * @throws Exception
     * @throws \Doctrine\DBAL\Driver\Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        try {
            $datas = Yaml::parseFile($this->kernel->getProjectDir().'/data.yml');
        } catch (ParseException $e) {
            $io->error($e->getMessage());

            return Command::FAILURE;
        }

        $ask = $io->ask('Voulez-vous vider la base de données avant l\'import ? [Y/n]', 'y');

        if ($ask === 'y') {
            $this->truncate();
            $filesystem = new Filesystem();
            $uploadDir = $this->kernel->getProjectDir().'/public/uploads/image/';
            if ($filesystem->exists($uploadDir)) {
                $filesystem->remove($uploadDir);
            }
            $invoicesDir = $this->kernel->getProjectDir().'/public/invoices';
            if ($filesystem->exists($invoicesDir)) {
                $filesystem->remove($invoicesDir);
            }

            $filesystem->mkdir($uploadDir);
        }

        $io->title('Import des données');
        $io->progressStart();

        if (array_key_exists('Categories', $datas)) {
            $this->loadCategories($datas['Categories'], $io);
        }

        if (array_key_exists('Utilisateurs', $datas)) {
            $this->loadUsers($datas['Utilisateurs'], $io);
        }

        $io->progressFinish();
        $io->success('Les données ont bien été importées');

        return Command::SUCCESS;
    }

    /**
     * @throws \Doctrine\DBAL\Driver\Exception
     * @throws Exception
     */
    private function truncate(): void
    {
        $connection = $this->em->getConnection();
        $platform = $connection->getDatabasePlatform();
        $this->em->getConnection()->prepare('SET FOREIGN_KEY_CHECKS = 0;')->executeQuery();
        foreach ($connection->getSchemaManager()->listTableNames() as $tableNames) {
            if ($tableNames != 'doctrine_migration_versions') {
                $connection->executeQuery($platform->getTruncateTableSQL($tableNames, true));
            }
        }
        $this->em->getConnection()->prepare('SET FOREIGN_KEY_CHECKS = 1;')->executeQuery();
    }

    private function loadCategories(array $categories, SymfonyStyle $io): void
    {
        foreach ($categories as $cat => $data) {
            $category = (new Category())->setName($cat);
            $this->em->persist($category);
            $io->progressAdvance();
            $this->em->flush();

            if (array_key_exists('locaux', $data)) {
                $this->loadRooms($data['locaux'], $category, $io);
            }
        }
    }

    private function loadRooms(array $locaux, Category $category, SymfonyStyle $io): void
    {
        foreach ($locaux as $local => $data) {
            $room = (new Room())
                ->setName($local)
                ->setIsActive(true)
                ->setSurface($data['surface'])
                ->setSeat($data['nbSiege'])
                ->setPrice($data['tarif'])
                ->setCategory($category)
                ->setDescription($data['desc'])
            ;

            $this->em->persist($room);
            $io->progressAdvance();
            $this->em->flush();

            if (array_key_exists('images', $data)) {
                $this->loadImages($data['images'], $room, $io);
            }
        }
    }

    private function loadImages(array $images, Room $room, SymfonyStyle $io): void
    {
        $filesystem = new Filesystem();
        foreach ($images as $image => $data) {
            try {
                $filesystem->copy('https://gitlab.com/apetrone/data-photo-1loc/-/raw/master/'.$image.'?inline=false', $this->kernel->getProjectDir().'/public/uploads/image/'.$image);
            } catch (IOException $e) {
                $io->error($e->getMessage());
            }

            $img = (new Image())
                ->setRoom($room)
                ->setPath($image)
                ->setUpdatedAt(new \DateTime())
                ->setName($data['alt'])
            ;

            $this->em->persist($img);
            $io->progressAdvance();
            $this->em->flush();
        }
    }

    private function loadUsers(array $utilisateurs, SymfonyStyle $io): void
    {
        foreach ($utilisateurs as $utilisateur => $data) {
            $comment = array_key_exists('commentaire', $data) ? $data['commentaire'] : null;
            $company = array_key_exists('entreprise', $data) ? $data['entreprise'] : null;
            $siret = array_key_exists('siret', $data) ? $data['siret'] : null;
            $mdp = array_key_exists('mdp', $data) ? $data['mdp'] : 'Azerty1234';
            $roles = array_key_exists('admin', $data) ? ['ROLE_USER', 'ROLE_ADMIN'] : ['ROLE_USER'];

            $user = (new User())
                ->setEmail($utilisateur)
                ->setRoles($roles)
                ->setSiret($siret)
                ->setComment($comment)
                ->setCompanyName($company)
                ->setAddress($data['adresse1'])
                ->setZipCode($data['cp'])
                ->setCity($data['ville'])
                ->setPhone($data['telephone'])
                ->setFirstName($data['prenom'])
                ->setLastName($data['nom'])
                ->setRegisterAt(new \DateTime($data['dateInscription']))
                ->setIsVerified(true)
            ;

            $user->setPassword($this->passwordHasher->hashPassword($user, $mdp));

            $this->em->persist($user);
            $io->progressAdvance();
            $this->em->flush();

            if (array_key_exists('reservations', $data)) {
                $this->loadReservation($data['reservations'], $user, $io);
            }
        }
    }

    private function loadReservation(array $reservations, User $user, SymfonyStyle $io): void
    {
        foreach ($reservations as $reservation => $data) {
            $room = '';
            $comment = array_key_exists('commentaire', $data) ? $data['commentaire'] : null;

            try {
                $room = $this->em->getReference(Room::class, $data['local']);
            } catch (ORMException $e) {
                $io->error($e->getMessage());
            }

            if ($room instanceof Room) {
                $resa = (new Reservation())
                    ->setRoom($room)
                    ->setUser($user)
                    ->setDateStart(new \DateTime($data['debut']))
                    ->setDateEnd(new \DateTime($data['fin']))
                    ->setComment($comment)
                    ->setStatus(0)
                ;

                $this->dispatcher->dispatch(new CreateReservationEvent($resa));

                $this->em->persist($user);
                $io->progressAdvance();
                $this->em->flush();
            }
        }
    }
}
