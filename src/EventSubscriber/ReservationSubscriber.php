<?php

namespace App\EventSubscriber;

use App\Event\CreateReservationEvent;
use App\Service\PdfHelper;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\KernelInterface;
use Twig\Environment;

class ReservationSubscriber implements EventSubscriberInterface
{
    private PdfHelper $pdfHelper;
    private EntityManagerInterface $entityManager;
    private KernelInterface $kernel;
    private Environment $environment;

    public function __construct(EntityManagerInterface $entityManager, PdfHelper $pdfHelper, KernelInterface $kernel, Environment $environment)
    {
        $this->entityManager = $entityManager;
        $this->pdfHelper = $pdfHelper;
        $this->kernel = $kernel;
        $this->environment = $environment;
    }

    public function onCreate(CreateReservationEvent $event): void
    {
        $reservation = $event->getReservation();

        $start = $reservation->getDateStart();
        $end = $reservation->getDateEnd();

        //Calcule du prix
        $duration = $start->diff($end);
        $hours = $duration->h + ($duration->days * 24);
        $total = $reservation->getRoom()->getPrice() * $hours;
        $reservation->setTotal($total);

        //Generation du code porte
        $now = (new \DateTime());
        $letter = ['A', 'B'];
        $code = mt_rand(0, 9).$now->format('N').$now->format('H').mt_rand(0, 9).$start->format('N').$letter[mt_rand(0, 1)];
        $reservation->setCode($code);

        $this->entityManager->persist($reservation);
        $this->entityManager->flush();

        // Génération du PDF
        $template = $this->environment->render('utils/invoice.html.twig', [
            'duration' => $hours,
            'reservation' => $reservation,
        ]);

        $name = 'facture-'.$reservation->getId().'.pdf';

        if ($this->kernel->getEnvironment() === 'test') {
            $name = 'test-facture-'.$reservation->getId().'.pdf';
        }

        $this->pdfHelper->generatePdf($name, $this->kernel->getProjectDir().'/public/invoices/', $template);
    }

    public static function getSubscribedEvents(): array
    {
        return [
            CreateReservationEvent::class => 'onCreate',
        ];
    }
}
