<?php

namespace App\Tests\EventSubscriber;

use App\Event\CreateReservationEvent;
use App\EventSubscriber\ReservationSubscriber;
use PHPUnit\Framework\TestCase;

class ReservationSubscriberTest extends TestCase
{
    public function testEventSubscription(): void
    {
        $this->assertArrayHaskey(CreateReservationEvent::class, ReservationSubscriber::getSubscribedEvents());
    }
}
