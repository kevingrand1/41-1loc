const imageMapResize = require('image-map-resizer')

function bindMap(selector) {
    const target = document.querySelector(selector)
    const areas = document.querySelectorAll(target.getAttribute('usemap') + ' *') //#rdc *
    const status = document.querySelector('.js-map').dataset.reserved.split(',')

    if (target.parentElement.querySelector('.js-area')) {
        target.parentElement.querySelector('.js-area').remove()
    }

    const areaZone = document.createElement('div')
    areaZone.classList.add('js-area')
    target.parentElement.appendChild(areaZone)

    areas.forEach(el => {
        let name = el.dataset.nom
        let className = 'bg-success'
        let text = 'libre'

        if (status.includes(name)) {
            className = 'bg-danger'
            text = 'occupée'
        }

        const badge = document.createElement('div')
        badge.setAttribute('title', text)
        badge.classList.add(className)
        areaZone.appendChild(badge)

        // badge.style.width = '20px'
        // badge.style.height = '20px'
        badge.style.position = 'absolute'

        let coords = el.getAttribute('coords').split(',')
        badge.style.left = coords[0] + 'px'
        badge.style.top = coords[1] + 'px'
        badge.style.width = coords[2] - coords[0] + 'px'
        badge.style.height = coords[3] - coords[1] + 'px'
        badge.style.zIndex = '400'
        badge.style.opacity = '0.5'

    })
}

function debounce(callback, delay){
    let timer;
    return function(){
        let args = arguments;
        let context = this;
        clearTimeout(timer);
        timer = setTimeout(function(){
            callback.apply(context, args);
        }, delay)
    }
}

imageMapResize()
bindMap('#p1')
bindMap('#p2')

window.addEventListener('resize', debounce(function()  {
    imageMapResize()
    bindMap('#p1')
    bindMap('#p2')
}, 500))

